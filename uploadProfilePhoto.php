<?php

$newProfilePhoto = $_FILES['profileImage'];

if (!(0 === $newProfilePhoto['error'])) {
    echo 'The file uploaded with a failure';
    die;
}

$imagesFiles = array_slice(scandir(__DIR__ . '/images'), 2);
foreach ($imagesFiles as $file) {
    if ('profile.jpg' === $file) {
        unlink(
            realpath(__DIR__ . '/images/profile.jpg')
        );
    }
}
move_uploaded_file(
    $newProfilePhoto['tmp_name'],
    __DIR__ . '/images/profile.' .
    pathinfo($newProfilePhoto['name'], PATHINFO_EXTENSION)
);
header('location: http://task9/');