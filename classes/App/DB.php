<?php

namespace App;

class DB
{

    private \PDO $dbh;
    private \PDOStatement $sth;

    /**
     * DB constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config)
    {
        if (!(isset($config['db']) && '' !== $config['db']
         && isset($config['host']) && '' !== $config['host']
         && isset($config['dbname']) && '' !== $config['dbname']
         && isset($config['username']) && '' !== $config['username']
         && isset($config['password']) && '' !== $config['password']
        )) {
            throw new \Exception('DB object was not created');
        }
        $this->dbh = new \PDO(
            $config['db'] . ':host=' . $config['host'] . ';dbname=' . $config['dbname'],
            $config['username'], $config['password']
        );
    }

    /**
     * Executes sql query
     * @param string $sql
     * @param array|null $params
     * @return bool
     * Returns true in success, false in failure
     */
    public function execute(string $sql, array $params = null): bool
    {
        $this->sth = $this->dbh->prepare($sql);
        return $this->sth->execute($params);
    }

    /**
     * Gets the response of sql query
     * @param string $sql
     * @param array|null $params
     * @return array|null
     * Returns data if executes with success, null if executes with failure
     */
    public function query(string $sql, ?array $params = null): ?array
    {
        if ($this->execute($sql, $params)) {
            return $this->sth->fetchAll();
        }
        return null;
    }
}