<?php

namespace App\Models;

use App\DB;
//require_once __DIR__ . '/../../../autoload.php';

//spl_autoload_register('autoload');

class GuestBook
{
    private ?array $notes;
    private DB $dbh;

    /**
     * GuestBook constructor.
     * @param array $config
     * Array of config for database
     */
    public function __construct(array $config)
    {
        try {

            $this->dbh = new DB($config);
            $sql = 'SELECT * FROM guestbook;';
            $notes = $this->dbh->query($sql);
            foreach ($notes as $elem) {
                $note = new \App\Models\Note(
                    (int)$elem['id'], $elem['name'],
                    (int)$elem['age'], $elem['dt'],
                    $elem['tm'], $elem['message']
                );
                $this->notes[] = $note;
            }

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Gets array of notes
     * @return array
     */
    public function getNotes(): array
    {
        return $this->notes;
    }

    /**
     * Adds new note to guestbook table
     * @param string $name
     * @param int $age
     * @param string $message
     * @return bool
     * @throws \Exception
     */
    public function addNewNote(
        string $name, int $age,
        string $message
    ): bool
    {
        $name = htmlspecialchars($name);
        $age = htmlspecialchars($age);
        $message = htmlspecialchars($message);
        if (empty($name) || empty($age) || empty($message)) {
            throw new \Exception('Can not add new note');
        }
        if (0 >= $age || 151 <= $age) {
            throw new \Exception('Error: incorrect age');
        }

        $sql = 'INSERT INTO guestbook
            (name, age, dt, tm, message)
            VALUES 
            (:name, :age, :date, :time, :message);';

        $date = new \DateTime('now');

        return $this->dbh->execute(
            $sql,
            [
                ':name' => $name,
                ':age' => $age,
                ':date' => $date->format('Y-m-d'),
                ':time' => $date->format('H:i:s'),
                ':message' => $message
            ]
        );

    }
}