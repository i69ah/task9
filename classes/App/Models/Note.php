<?php
namespace App\Models;
class Note
{
    private int $id;
    private string $name;
    private int $age;
    private string $date;
    private string $time;
    private string $message;

    /**
     * Note constructor.
     * @param int $id
     * @param string $name
     * @param int $age
     * @param string $date
     * @param string $time
     * @param string $message
     * @throws Exception
     * if at least one of params is empty
     */
    public function __construct(
        int $id, string $name, int $age,
        string $date, string $time, string $message
    )
    {
        $id = htmlspecialchars($id);
        $name = htmlspecialchars($name);
        $age = htmlspecialchars($age);
        $date = htmlspecialchars($date);
        $time = htmlspecialchars($time);
        $message = htmlspecialchars($message);
        if (
            empty($id) || empty($name) || empty($age) ||
            empty($date) || empty($time) || empty($message)
        ) {
            throw new Exception('Failed to construct new Note object');
        }
        $this->id = htmlspecialchars($id);
        $this->name = htmlspecialchars($name);
        $this->age = htmlspecialchars($age);
        $this->date = htmlspecialchars($date);
        $this->time = htmlspecialchars($time);
        $this->message = htmlspecialchars($message);
    }

    /**
     * Get array of notes fields
     * @return array
     */
    public function getNote(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'age' => $this->age,
            'date' => $this->date,
            'time' => $this->time,
            'message' => $this->message,
        ];
    }
}