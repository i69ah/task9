<?php

namespace App;

class View
{
    protected array $data = [];

    /**
     * View constructor.
     * @param string $name
     * Name of parameter
     * @param $value
     * Value of parameter
     * @param string $template
     * Template to display
     * @param string $style
     * Name of css styles file
     */
    public function  __construct(
        string $name, $value,
        string $template, string $style)
    {

        if (!('' !== $name && !empty($value)
            && is_file(realpath(__DIR__ . '/../../templates/' . $template . '.php'))
            && is_file(realpath(__DIR__ . '/../../styles/' . $style . '.css')))
        ) {
            throw new \Exception('View object was not created');
        }

        ob_start();
        include __DIR__ . '/../../styles/' . $style . '.css';
        $this->data['style'] = ob_get_contents();
        ob_end_clean();

        $this->data[$name] = $value;

        ob_start();
        include __DIR__ . '/../../templates/' . $template . '.php';
        $this->data['template'] = ob_get_contents();
        ob_end_clean();
    }

    /**
     * Renders template specified in constructor
     * @return string
     * Returns the template with data
     */
    public function render(): string
    {
        return $this->data['template'];
    }
}