<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        <?= $this->data['style'] ?>
    </style>
    <title>Admin</title>
</head>
<body>
<div class="content">
    <div class="wrapper">
        <header class="hf">
            <div class="header__element">
                <a href="/">About</a>
            </div>
            <div class="header__element">
                <a href="/admin.php">Admin</a>
            </div>
            <div class="header__element">
                <a href="/gallery.php">Gallery</a>
            </div>
            <div class="header__element">
                <a href="/guestbook.php">Guest Book</a>
            </div>
        </header>
        <main>
            <div class="photo_and_about">
                <div class="photo">
                    <div class="photo__title">
                        Update your profile photo
                    </div>
                    <div class="photo__current">
                        <img src="/images/profile.jpg" alt="">
                    </div>
                    <form action="/uploadProfilePhoto.php" enctype="multipart/form-data" method="post">
                        <input type="file" name="profileImage">
                        <button type="submit">Submit</button>
                    </form>
                </div>
                <div class="about">
                    <div class="about__title">
                        Change information about yourself
                    </div>
                    <form action="/updateAboutText.php" method="post">
                        <textarea name="aboutText" id="" cols="60" rows="15"><?php
                        echo $this->data['data'];
                            ?></textarea>
                        <button type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </main>
        <footer class="hf">
            Footer
        </footer>
    </div>
</div>
</body>
</html>