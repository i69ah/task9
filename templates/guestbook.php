<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        <?= $this->data['style'] ?>
    </style>
    <title>Guest Book</title>
</head>
<body>
<div class="content">
    <div class="wrapper">
        <header class="hf">
            <div class="header__element">
                <a href="/">About</a>
            </div>
            <div class="header__element">
                <a href="/admin.php">Admin</a>
            </div>
            <div class="header__element">
                <a href="/gallery.php">Gallery</a>
            </div>
            <div class="header__element">
                <a href="/guestbook.php">Guest Book</a>
            </div>
        </header>
        <main>
            <div class="notes">
                <?php
                foreach ($this->data['data'] as $note) {
                    ?>
                    <div class="note" id="<?= $note->getNote()['id'] ?>">
                        <div class="note__title">
                            <div class="note__name"><?= $note->getNote()['name'] ?></div>
                            <div class="note__time"><?= $note->getNote()['time'] ?></div>
                            <div class="note__date"><?= $note->getNote()['date'] ?></div>
                        </div>
                        <div class="note__divider"></div>
                        <div class="note__message">
                            <?= $note->getNote()['message'] ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="addNewNote">
                <form action="/addNewNote.php" method="post">
                    <input type="text" name="name" placeholder="Your name">
                    <input type="text" name="message" placeholder="Your message">
                    <input type="text" name="age" placeholder="Your age">
                    <button type="submit">Submit</button>
                </form>
            </div>
        </main>
        <footer class="hf">
            Footer
        </footer>
    </div>
</div>
</body>
</html>