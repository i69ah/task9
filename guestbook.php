<?php

require_once __DIR__ . '/autoload.php';

spl_autoload_register('autoload');

$config = json_decode(
    file_get_contents(__DIR__ . '/dbconfig.json'),
    true
);

$guestBook = new \App\Models\GuestBook($config);
try {

    $viewGuestbook = new \App\View(
      'data', $guestBook->getNotes(),
      'guestbook', 'guestbook'
    );
    echo $viewGuestbook->render();

} catch (Exception $e) {
    echo $e->getMessage();
}
//try {
//
//    $dbh = new \App\DB($config);
//
//    $sql = 'SELECT * FROM guestbook;';
//
//    $data = $dbh->query($sql);
//
//    $viewGuestbook = new \App\View(
//        'data', $data,
//        'guestbook', 'guestbook'
//    );
//    echo $viewGuestbook->render();
//
//} catch (Exception $e) {
//    echo $e->getMessage();
//}