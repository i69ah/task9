<?php

require_once __DIR__ . '/autoload.php';

spl_autoload_register('autoload');

$data = array_slice(scandir(__DIR__ . '/images/gallery'), 2);

try {

    $adminView = new \App\View(
        'data', $data,
        'gallery', 'gallery'
    );
    echo $adminView->render();

} catch (Exception $e) {
    echo $e->getMessage();
}