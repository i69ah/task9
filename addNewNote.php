<?php

require_once __DIR__ . '/autoload.php';

spl_autoload_register('autoload');

$name = htmlspecialchars($_POST['name']);
$message = htmlspecialchars($_POST['message']);
$age = (int)htmlspecialchars($_POST['age']);

try {

    if (empty($age) || empty($name) || empty($message)) {
        throw new Exception('Error: name or message or age is undefined');
    }
    if (0 >= $age || 151 <= $age) {
        throw new Exception('Error: incorrect age');
    }

    $config = json_decode(
        file_get_contents(__DIR__ . '/dbconfig.json'),
        true
    );

    $guestBook = new \App\Models\GuestBook($config);
    $guestBook->addNewNote($name, $age, $message);

    header('location: http://task9/guestbook.php');

} catch (Exception $e) {
    echo $e->getMessage();
}