<?php

$newPhoto = $_FILES['newPhoto'];

if (empty($newPhoto) || !(0 === $newPhoto['error'])) {
    echo 'The file uploaded with a failure';
    die;
}

move_uploaded_file(
  $newPhoto['tmp_name'],
  __DIR__ . '/images/gallery/' . $newPhoto['name']
);

header('location: http://task9/gallery.php');