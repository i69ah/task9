<?php

require_once __DIR__ . '/autoload.php';

spl_autoload_register('autoload');

$aboutText = htmlspecialchars($_POST['aboutText']);

try {

    if (empty($aboutText)) {
        throw new Exception('New about text is empty');
    }

    $config = json_decode(
        file_get_contents(__DIR__ . '/dbconfig.json'),
        true
    );

    $dbh = new \App\DB($config);

    $sql = 'UPDATE about SET about=:about WHERE id=1';

    $success = $dbh->execute($sql, [':about' => $aboutText]);

    header('location: http://task9/');

} catch (Exception $e) {
    echo $e->getMessage();
}
