<?php

require_once __DIR__ . '/autoload.php';

spl_autoload_register('autoload');

$config = json_decode(
    file_get_contents(__DIR__ . '/dbconfig.json'),
    true
);

try {

    $dbh = new \App\DB($config);

    $sql = 'SELECT * FROM about;';

    $data = $dbh->query($sql);

    $adminView = new \App\View(
        'data', $data[0]['about'],
        'admin', 'admin'
    );
    echo $adminView->render();

} catch (Exception $e) {
    echo $e->getMessage();
}